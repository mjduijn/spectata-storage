#!/usr/bin/env bash

cp $CASSANDRA_CONFIG/cassandra.yaml $CASSANDRA_CONFIG/cassandra.yaml.backup

sed -i -e "s/num_tokens/\#num_tokens/" $CASSANDRA_CONFIG/cassandra.yaml

echo 'root - memlock unlimited' >> /etc/security/limits.conf
echo 'root - nofile 100000' >> /etc/security/limits.conf
echo 'root - nproc 32768' >> /etc/security/limits.conf
echo 'root - as unlimited' >> /etc/security/limits.conf

echo '* - nproc 32768' >> /etc/security/limits.d/90-nproc.conf

echo 'vm.max_map_count = 131072' >> /etc/sysctl.conf

sysctl -p