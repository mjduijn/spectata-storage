Spectata Storage
======

This project is the storage component of the Spectata application.
It is implemented as single node Cassandra Database cluster.
The node is packaged in a docker container and files for building and running are included.
To run it first build and then run the container using docker-compose.