#!/usr/bin/env bash

#Wait for DB to be initialized
sleep 20s

echo 'Running DB initialization'
cqlsh -f /scripts/init.cql