FROM base-java
MAINTAINER maartenduijn@msn.com

#Install Prerequisites
RUN yum install -y jemalloc && yum clean all

#Install JNA
ENV JNA_VERSION 4.2.2
RUN mkdir -p $JAVA_HOME/lib \
    && wget -P /opt/jdk/lib/ https://github.com/twall/jna/raw/$JNA_VERSION/dist/linux-x86-64.jar

#Install Cassandra
ENV CASSANDRA_VERSION=3.0.8 \
	CASSANDRA_HOME=/opt/cassandra

ENV CASSANDRA_CONFIG=$CASSANDRA_HOME/conf \
	PATH=$PATH:$CASSANDRA_HOME/bin

RUN wget -q http://apache.cs.uu.nl/cassandra/$CASSANDRA_VERSION/apache-cassandra-$CASSANDRA_VERSION-bin.tar.gz -O /tmp/cassandra.tar.gz \
    && tar zxf /tmp/cassandra.tar.gz --directory=/opt \
    && ln -s /opt/apache-cassandra-$CASSANDRA_VERSION/ /opt/cassandra \
    && rm -rf /tmp/*

EXPOSE 7000 7001 9160 9042 7199
CMD ["/scripts/cassandra-singlenode.sh"]

COPY scripts /scripts
RUN chmod +x /scripts -R
