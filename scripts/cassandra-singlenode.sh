#!/usr/bin/env bash

mkdir -p /shared/cassandra/data
mkdir -p /shared/cassandra/commitlog
mkdir -p /opt/cassandra/data/saved_caches

#Set cassandra storage to ceph storage
# "# data_file_directories:" -> "data_file_directories:"
sed -i -e "s/^# data_file_directories:/data_file_directories:/" $CASSANDRA_CONFIG/cassandra.yaml
# "#     - /var/lib/cassandra/data" -> "    - /shared/cassandra/data"
sed -i -e "s/^#     - \/var\/lib\/cassandra\/data/    - \/shared\/cassandra\/data/" $CASSANDRA_CONFIG/cassandra.yaml
# "# commitlog_directory: /var/lib/cassandra/commitlog" -> commitlog_directory:/shared/cassandra/commitlog
sed -i -e "s/^# commitlog_directory:.*/commitlog_directory: \/shared\/cassandra\/commitlog/" $CASSANDRA_CONFIG/cassandra.yaml

# Get running container's IP
IP=`hostname --ip-address`
if [ $# == 1 ]; then SEEDS="$1,$IP";
else SEEDS="$IP"; fi

# 0.0.0.0 Listens on all configured interfaces
# but you must set the broadcast_rpc_address to a value other than 0.0.0.0
sed -i -e "s/^rpc_address.*/rpc_address: 0.0.0.0/" $CASSANDRA_CONFIG/cassandra.yaml

sed -i -e "s/# broadcast_address.*/broadcast_address: $IP/" $CASSANDRA_CONFIG/cassandra.yaml

sed -i -e "s/# broadcast_rpc_address.*/broadcast_rpc_address: $IP/" $CASSANDRA_CONFIG/cassandra.yaml

# Be your own seed
sed -i -e "s/- seeds: \"127.0.0.1\"/- seeds: \"$SEEDS\"/" $CASSANDRA_CONFIG/cassandra.yaml

# Listen on IP:port of the container
sed -i -e "s/^listen_address.*/listen_address: $IP/" $CASSANDRA_CONFIG/cassandra.yaml

# With virtual nodes disabled, we need to manually specify the token
echo "JVM_OPTS=\"\$JVM_OPTS -Dcassandra.initial_token=0\"" >> $CASSANDRA_CONFIG/cassandra-env.sh

# Pointless in one-node cluster, saves about 5 sec waiting time
echo "JVM_OPTS=\"\$JVM_OPTS -Dcassandra.skip_wait_for_gossip_to_settle=0\"" >> $CASSANDRA_CONFIG/cassandra-env.sh

# Most likely not needed
echo "JVM_OPTS=\"\$JVM_OPTS -Djava.rmi.server.hostname=$IP\"" >> $CASSANDRA_CONFIG/cassandra-env.sh

# If configured in $CASSANDRA_DC, set the cassandra datacenter.
if [ ! -z "$CASSANDRA_DC" ]; then
    sed -i -e "s/endpoint_snitch: SimpleSnitch/endpoint_snitch: PropertyFileSnitch/" $CASSANDRA_CONFIG/cassandra.yaml
    echo "default=$CASSANDRA_DC:rac1" > $CASSANDRA_CONFIG/cassandra-topology.properties
fi

echo '[INFO] Configuring cassandra'
/scripts/cassandra-config.sh

echo '[INFO] Running init db script'
/scripts/init-db.sh &

echo '[INFO] Running cassandra'
/opt/cassandra/bin/cassandra -f -R